def soma(a: int, b: int):
    """
    soma
    ---
    Sum two values

    Args:
    ---
        a (int): first number
        b (int): second number

    Returns:
    ---
        int: a + b
    
    Examples
    ---
    >>> print(soma(1, 10))
    >>> 11
    """
    return a + b